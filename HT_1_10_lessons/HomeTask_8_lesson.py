import array
import time


# 1 ------------------------------------------------------------------------------------
# def time_counter(func):
#     def wrapper(*args, **kwargs):
#         start = time.monotonic()
#         result = func(*args, **kwargs)
#         end = time.monotonic()
#         print(f'Time of "{func.__name__}" execution = {round((end - start), 2)} sec.')
#         return result
#
#     return wrapper
#
#
# @time_counter
# def my_function(n=5):
#     while n:
#         print(f'{n} - {time.ctime()}')
#         time.sleep(1)
#         n -= 1
#
#
# my_function(3)
#
# 2 ------------------------------------------------------------------------------------
# user_value = int(input('Enter the length please: '))
# first_value = user_value*3
# my_array = array.array('L')
# while first_value:
#     if first_value % 3 == 0:
#         my_array.append(first_value)
#     first_value -= 1
# print(my_array.tolist())
#
# 3 ------------------------------------------------------------------------------------
# def skip(func):
#     def wrapper(*args, **kwargs):
#         if args or kwargs:
#             print(f'\nParameters of function {func.__name__}')
#             if args: print(f'args = {args}')
#             if kwargs: print(f'kwargs = {kwargs}')
#         return None
#     return wrapper
#
#
# @skip
# def my_function(param=None):
#     print('Hello world')
#
#
# my_function()
# my_function(5)
# my_function(param='Hello')
# my_function(10, 15, param='Hello')
#
# 4 ------------------------------------------------------------------------------------
# def my_generator(end=True):
#     step = 1
#     while end:
#         step += 1
#         yield sum(list(range(1, step)))
#         if str(end) != 'True':
#             end -= 1
#
#
# gen = list(my_generator(10))
# print(gen)
#
# gen = my_generator()
# print(gen)
# for i in range(1, 100):
#     print(next(gen))
#
