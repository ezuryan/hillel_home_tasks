from re import findall, sub, match, search
from datetime import date


def read_text_from_file(path):
    text_from_file = ''
    try:
        with open(path, 'r') as file_to_read:
            text_from_file = file_to_read.read()
    except FileNotFoundError:
        print('FileNotFoundError. Stop')
    return text_from_file


# user_path = 'Files_for_tests/text_for HT_7'

# 1 ------------------------------------------------------------------------------------
# """
# переделал то что отправил как ДЗ
# """
#
# # user_path = input('Enter full file path: ')
# text_to_work = read_text_from_file(user_path)
# # pattern = r'\d{2}-[01][0-9]-\d{4}'
# pattern = r'([0-2][0-9]-[01][0-9]-\d{4})|(3[01]-[01][0-9]-\d{4})'
# if text_to_work:
#     m = findall(pattern, text_to_work)
#     for items in m:
#         print(items)
#
# else:
#     print('File empty. Stop')
#
# 2 ------------------------------------------------------------------------------------
# user_year = int(input('Enter year to check it: '))
# try:
#     date(user_year, 2, 29)
#     print(f'{user_year} - is a leap year')
# except:
#     print(f'{user_year} - is not a leap year')
#
# 3 ------------------------------------------------------------------------------------
# """
# так как print(int('-07')) выдает -7, в коде распознаю числа так же
# """
# user_path = input('Enter full file path: ')
# text_to_work = read_text_from_file(user_path)
# if text_to_work:
#     pattern = r'-?\d+\.?\d*'
#     res_list = findall(pattern, text_to_work)
#     res_sum = 0
#     for item in res_list:
#         res_sum += float(item)
#     print(f'Sum = {res_sum}')
# else:
#     print('File empty. Stop')
#
# 4 ------------------------------------------------------------------------------------
# def check_value(_f, _d, _l):
#     flag = False
#
#     try:
#         _f = float(_f)
#         flag = True
#     except:
#         print(f'\nError first element value: {_f}')
#         return False
#     try:
#         _d = float(_d)
#         flag = True
#     except:
#         print(f'\nError denominator value: {_d}')
#         return False
#     try:
#         _l = int(_l)
#         if _l > 0:
#             flag = True
#         else:
#             print(f'\nError length value: {_l}')
#             return False
#     except:
#         print(f'\nError length value: {_l}')
#         return False
#
#     return flag
#
#
# first = input('Enter first element of progression: ')
# denom = input('Enter denominator of progression: ')
# length = input('Enter length of progression: ')
#
# if check_value(first, denom, length):
#     length = int(length)
#     first = float(first)
#     denom = float(denom)
#     if first.is_integer() and denom.is_integer():
#         first = int(first)
#         denom = int(denom)
#
#     progression = [first]
#     n = 2
#     while n <= length:
#         progression.append(progression[n - 2] * denom)
#         n += 1
#
#     print(f'\nResult:\n{progression}')
#
# 5 ------------------------------------------------------------------------------------
# user_path = input('Enter full file path: ')
# text_to_work = read_text_from_file(user_path)
# if text_to_work:
#     pattern = r'(-0?\d+\.?\d+)|(-[1-9])'  # спасибо за статейки, кажется начал понимать немного )))
#     result = sub(pattern, ' ', text_to_work)
#     print(result)
# else:
#     print('File empty. Stop')
#
# 6 ------------------------------------------------------------------------------------
# user_path = input('Enter full file path: ')
# text_to_work = read_text_from_file(user_path)
# if text_to_work:
#     pattern = r'[A-Z][a-z]+'
#     res_list = findall(pattern, text_to_work)
#     print(res_list)
# else:
#     print('File empty. Stop')
#
# переделал задание 4-------------------------------------------------------------------
# def check_value(_f, _d, _l):
#     _f = str(_f)
#     _d = str(_d)
#     _l = str(_l)
#     flag = False
#
#     try:
#         _f = float(_f)
#         flag = True
#     except:
#         print(f'\nError first element value: {_f}')
#         return False
#     try:
#         _d = float(_d)
#         flag = True
#     except:
#         print(f'\nError denominator value: {_d}')
#         return False
#     try:
#         _l = int(_l)
#         if _l > 0:
#             flag = True
#         else:
#             print(f'\nError length value: {_l}')
#             return False
#     except:
#         print(f'\nError length value: {_l}')
#         return False
#
#     if flag:
#         _l = int(_l)
#         _f = float(_f)
#         _d = float(_d)
#         if _f.is_integer() and _d.is_integer():
#             _f = int(_f)
#             _d = int(_d)
#
#         return [_f, _d, _l]
# def my_generator(first: float, denom: float, length: int):
#     res_check = check_value(first, denom, length)
#     if res_check:
#         first, denom, length = res_check
#         next_value = first
#         n = 1
#         while n <= length:
#             yield next_value
#             next_value = next_value * denom
#             n += 1
# res = my_generator(1, 2, 'd')
# print(res)
# for i in res:
#     print(i)
#
# for i in my_generator(1, 2, 10):
#     print(i)
