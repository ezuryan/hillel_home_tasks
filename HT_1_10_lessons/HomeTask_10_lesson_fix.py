import os
import pickle


class HomeLibraries:
    def __init__(self):
        self.name = self.__class__.__name__
        self.books = list()

    def __get_books(self, sorting_key=None, **kwargs):
        res_list = list()
        if len(self.books):
            _value_key = str()
            kwargs_key_list_correct = list()

            for kwargs_key, kwargs_v in kwargs.items():
                if self.books[0].get(kwargs_key, False):
                    _value_key += str(kwargs_v).upper()
                    kwargs_key_list_correct.append(kwargs_key)

            if not kwargs_key_list_correct and len(kwargs):
                print(f'Parameter error {kwargs}')
                return res_list

            while len(kwargs_key_list_correct) <= len(self.books[0].keys()):
                kwargs_key_list_correct.append('')

            res_list = list(
                filter(lambda x:
                       str(str(x.get(kwargs_key_list_correct[0], '')) +
                           str(x.get(kwargs_key_list_correct[1], '')) +
                           str(x.get(kwargs_key_list_correct[2], '')) +
                           str(x.get(kwargs_key_list_correct[3], '')) +
                           str(x.get(kwargs_key_list_correct[4], ''))
                           ).upper()
                       == _value_key, self.books))

            if sorting_key:
                if self.books[0].get(sorting_key):
                    res_list = sorted(res_list, key=lambda x: (x[str(sorting_key).lower()]))
                else:
                    print(f'Sorting key error. Sorted by default key "name"')
                    res_list = sorted(res_list, key=lambda x: (x['name']))

        return res_list

    def add_book(self, name: str, author: str, genre: str, year: int, text_of_the_book: str):
        if name and author and genre and year and text_of_the_book:
            book = self.__get_books(name=name, author=author, genre=genre, year=year, text_of_the_book=text_of_the_book)
            if book:
                print(f'\nBook already in library:\n"{book[0]}" ')
            else:
                self.books.append(
                    {
                        'name': str(name.upper()),
                        'author': str(author.title()),
                        'genre': str(genre.upper()),
                        'year': str(year),
                        'text_of_the_book': str(text_of_the_book)
                    }
                )
                print(f'\nBook added:\n"{self.books[-1]}"')
        else:
            print(f'\nNot enough data to add book')

    def delete_book(self, **kwargs):
        books = self.__get_books(**kwargs)
        if not books and len(self.books):
            print(f'\nBook not found')
        elif not len(self.books):
            print(f'\nEmpty library')

        for book in books:
            self.books.remove(book)
            print(f'\nBook deleted:\n"{book}"')

    def get_books(self, sorting_key=None, **kwargs):
        books = self.__get_books(sorting_key, **kwargs)
        if not books and len(self.books):
            print(f'Book not found')
        elif not len(self.books):
            print(f'Empty library')

        return books

    def book_detail(self, **kwargs):
        books = self.__get_books(**kwargs)
        for book in books:
            print('\nDetails:')
            for k, v in book.items():
                print(f"{str(k).upper()} - {v}")

    def introduction(self, return_books=False):
        print(f'\nThis is a library of "{self.name}"\n'
              f'It has {len(self.books)} books')
        if return_books:
            return self.books

    def save_dump(self):
        file_name = f'{self.__class__.__name__}.dump'
        with open(f'{file_name}', 'wb') as dump_file:
            pickle.dump(self, dump_file)
            print(f'\nFile "{file_name}" saved')

    def load_dump(self):
        file_name = f'{self.__class__.__name__}.dump'
        if os.path.exists(f'{file_name}'):
            with open(f'{file_name}', 'rb') as dump_file:
                self = pickle.load(dump_file)
        else:
            print('No dump file')
        return self


if __name__ == '__main__':
    my_personal_library = HomeLibraries().load_dump()

    # наполнение
    my_personal_library.add_book('book6', 'author 1', 'genre1', 2001, 'txt1')
    my_personal_library.add_book('book5', 'author 1', 'genre1', 2002, 'txt2')
    my_personal_library.add_book('book4', 'author 1', 'genre2', 2002, 'txt3')
    my_personal_library.add_book('book3', 'author 2', 'genre2', 2003, 'txt4')
    my_personal_library.add_book('book2', 'author 2', 'genre1', 2002, 'txt5')
    my_personal_library.add_book('book1', 'author 3', 'genre3', 2005, 'txt6')
    my_personal_library.add_book('book1', 'author 1', 'genre1', 2002, 'txt11')

    # функции
    my_personal_library.save_dump()

    print('----------------')
    books = my_personal_library.get_books(sorting_key='author', genre='genre1', year='2002')
    for _book in books:
        print(_book)

    my_personal_library.delete_book(name='book1', year='2010')

    my_personal_library.add_book('book1', 'author 1', 'genre1', 2001, 'txt1')

    print('----------------')
    books = my_personal_library.get_books(sorting_key='year', genre='genre1')
    for _book in books:
        print(_book)

    # print('----------------')
    # books = my_personal_library.get_books()  # вывод всех книг
    # for book in books:
    #     print(book)
    #
    # print('----------------')
    # books = my_personal_library.get_books(sorting_key='year')
    # for _book in books:
    #     print(_book)
    #
    # my_personal_library.book_detail(name='book1')
    #
    # my_personal_library.introduction()
    #
    # print('----------------')
    # books = my_personal_library.introduction(True)
    # for book in books:
    #     print(book)
