def possible_steps(_y, _x, _y_last, _x_last):
    res_list = list()

    if _x > 0:
        if matrix_list[_y][_x - 1] == 1:
            res_list.append((_y, _x - 1))
    if _y > 0:
        if matrix_list[_y - 1][_x] == 1:
            res_list.append((_y - 1, _x))
    if _x < matrix_length_x - 1:
        if matrix_list[_y][_x + 1] == 1:
            res_list.append((_y, _x + 1))
    if _y < matrix_length_y - 1:
        if matrix_list[_y + 1][_x] == 1:
            res_list.append((_y + 1, _x))

    try:
        i = res_list.index((_y_last, _x_last))
        res_list.pop(i)
    except:
        pass

    return res_list


def check_exit(_y, _x):
    if matrix_list[_y][_x] == 1 and (_y, _x) != start_coords_y_x_:
        if _y == 0 or _x == 0 or _y == matrix_length_y - 1 or _x == matrix_length_x - 1:
            print('')
            print((_y, _x), ' - Exit')
            return 1
        else:
            # print((_y, _x), ' - Dead end')
            return 0
    else:
        # print((_y, _x), ' - wall')
        return 0


def step_back():
    _flag = True
    while _flag:
        if len(list_of_multi_steps) > 0:
            tmp_item = step_list.pop()

            if tmp_item == list_of_multi_steps[-1]:
                step_list.append(tmp_item)
                list_of_multi_steps.pop()
                _flag = False
            else:
                matrix_list[tmp_item[0]][tmp_item[1]] = 0

            _y_last = step_list[-2][0]
            _x_last = step_list[-2][1]
            _y = step_list[-1][0]
            _x = step_list[-1][1]

            return _y, _x, _y_last, _x_last
        else:
            print('No exit in this lab')
            _flag = False


if __name__ == '__main__':
    lab = [
        ['#', '#', '#', '#', '#', '#', '#', '#'],
        ['#', '.', '.', '.', '.', '.', '.', '#'],
        ['.', '.', '#', '#', '.', '#', '.', '#'],
        ['#', '.', '.', '.', '.', '#', '.', '#'],
        ['#', '.', '#', '#', '.', '#', '.', '#'],
        ['#', '.', '.', '.', '.', '#', '.', '#'],
        ['#', '#', '.', '#', '.', '#', '.', '#'],
        ['#', '.', '.', '.', '.', '#', '.', '#'],
        ['#', '.', '#', '#', '.', '#', '.', '#'],
        ['#', '.', '.', '.', '.', '.', '.', '#'],
        ['#', '#', '#', '#', '.', '#', '#', '#'],
    ]
    start_coords_y_x_ = (2, 0)

    # ----------------------------------------------------------------
    symbol_code = lab[start_coords_y_x_[0]][start_coords_y_x_[1]]
    matrix_length_y = len(lab)
    matrix_length_x = len(lab[0])
    matrix_list = list()
    for y in range(0, matrix_length_y):
        matrix_list.append([])
        for x in range(0, matrix_length_x):
            if lab[y][x] == symbol_code:
                matrix_list[y].append(1)
            else:
                matrix_list[y].append(0)

    # ----------------------------------------------------------------
    y = start_coords_y_x_[0]
    x = start_coords_y_x_[1]
    y_last = y
    x_last = x
    step_list = [(y, x)]
    list_of_multi_steps = list()

    # ----------------------------------------------------------------
    flag = True
    while flag:
        possible_steps_res = possible_steps(y, x, y_last, x_last)

        if len(possible_steps_res) > 1:
            list_of_multi_steps.append((y, x))

        if len(possible_steps_res) > 0:
            for items in possible_steps_res:
                try:
                    tmp_i = step_list.index(items)
                    # print('ходим по кругу, уже были - ', y, x, ' - ', items)
                    y, x, y_last, x_last = step_back()
                    continue
                except:
                    step_list.append(items)
                    y_last = y
                    x_last = x
                    y = step_list[-1][0]
                    x = step_list[-1][1]
                    break

            print('step_list', step_list)
        else:
            if check_exit(y, x):
                flag = False
                continue

            try:
                y, x, y_last, x_last = step_back()
            except:
                flag = False
