# 1 ------------------------------------------------------------------------------------
# def conversion(value_before, type_of_conversion):
#     # type_of_conversion = 1 arabic in romain
#     # type_of_conversion = 0 romain in arabic
#     cipher_dict = {1000: 'M',
#                    900: 'CM',
#                    500: 'D',
#                    400: 'CD',
#                    100: 'C',
#                    90: 'XC',
#                    50: 'L',
#                    40: 'XL',
#                    10: 'X',
#                    9: 'IX',
#                    5: 'V',
#                    4: 'IV',
#                    1: 'I'}
#
#     if type_of_conversion:
#         res_str = str()
#         for item in cipher_dict.items():
#             number = item[0]
#             letter = item[1]
#             while value_before >= number:
#                 res_str = res_str + letter
#                 value_before = value_before - number
#
#         return res_str
#     else:
#         cipher_dict = {v: k for k, v in cipher_dict.items()}
#         t_list = list(value_before)
#         res_num = 0
#         while t_list:
#             letter = ''.join(t_list[0:2])
#             number = cipher_dict.get(letter)
#             if not number:
#                 letter = ''.join(t_list[0:1])
#                 number = cipher_dict.get(letter)
#
#             t_list = t_list[len(letter)::]
#             res_num += number
#
#         return res_num
#
#
# for number_arabic in range(1, 4000):
#     res = conversion(number_arabic, 1)
#     print(number_arabic, ' - ', res)
#
# res = conversion('DXCVIII', 0)
# print('')
# print('DXCVIII = 598', ' - ', res)
#
# 2 ------------------------------------------------------------------------------------
# def no_numbers(path):
#     try:
#         with open(path, 'r+') as file_before:
#             text_before = file_before.read()
#             if text_before:
#                 for i in range(0, 10):
#                     text_before = text_before.replace(str(i), '')
#
#                 file_before.write('\n--------------------------\n\n')
#                 file_before.write(text_before)
#                 print('Success')
#             else:
#                 print('File is empty. Program stopped')
#     except FileNotFoundError:
#         print('FileNotFoundError. Program stopped')
#
#
# user_path = input('Enter full file path: ')
# no_numbers(user_path)
#
# 3 ------------------------------------------------------------------------------------
# import string
#
#
# def cipher_text(text, cipher):
#     text = text.upper()
#     for item in cipher.items():
#         text = text.replace(item[0], item[1].lower())
#
#     text = text.upper()
#     return text
#
#
# def make_cipher_dict(bias, direction):
#     _alfabet = string.ascii_uppercase
#     res_dict = dict()
#     if bias < len(_alfabet):
#         _alfabet_cipher = _alfabet[bias:] + _alfabet[:bias]
#         i = 0
#         if direction:
#             while i <= len(_alfabet) - 1:
#                 res_dict.update({_alfabet[i]: _alfabet_cipher[i]})
#                 i += 1
#         else:
#             while i <= len(_alfabet) - 1:
#                 res_dict.update({_alfabet_cipher[i]: _alfabet[i]})
#                 i += 1
#
#     return res_dict
#
#
# def cipher_func(bias, path, direction):
#     # direction == True from arabic in Romanian
#     # direction == False backwards
#
#     if bias.isdigit():
#         bias = int(bias)
#         cipher_dict = make_cipher_dict(bias, direction)
#         if cipher_dict:
#             try:
#                 with open(path, 'r+') as file_before:
#                     text_before = file_before.read()
#                     if text_before:
#                         text_after = cipher_text(text_before, cipher_dict)
#                         file_before.write('\n--------------------------\n\n')
#                         file_before.write(text_after)
#                         print('Success')
#                     else:
#                         print('File is empty. Program stopped')
#             except FileNotFoundError:
#                 print('FileNotFoundError. Program stopped')
#         else:
#             print('No cipher dict. Program stopped')
#     else:
#         print('Bias value error. Program stopped')
#
#
# user_bias = input('Enter bias value: ')
# user_path = input('Enter full file path: ')
# cipher_func(user_bias, user_path, False)
#

