class MovesMixin:
    def goes_to(self, destination):
        print(f'{self.kind_of} "{self.name}" is heading to {destination}.')
        self.stopping_places.append(destination)

    def get_all_stopping_places(self):
        print(f'{self.kind_of} "{self.name}" has visited {self.stopping_places}')


class Transport:
    def __init__(self, name, kind_of, environment):
        self.kind_of = str(kind_of).capitalize()
        self.name = str(name)
        self.environment = str(environment)
        self.stopping_places = list()

    def introduce(self):
        print(f'{self.kind_of} "{self.name}". Designed for {self.environment}.')

    def get_all_info(self):
        all_info = self.__dict__
        print('------Info: ------')
        for key, value in all_info.items():
            print(f'{key}: {value}')
        print('------------------')


class Car(Transport, MovesMixin):
    def __init__(self, name, color='White', body_type='Sedan'):
        super(Car, self).__init__(name=name, kind_of=self.__class__.__name__, environment='road')
        self.color = color
        self.body_type = body_type

    def beep(self, repeat_times):
        while repeat_times:
            print(f'{self.kind_of} "{self.name}" beeps!!!')
            repeat_times -= 1

    def blink(self, side):
        print(f'{self.kind_of} "{self.name}" blinks {side}')


class Plane(Transport, MovesMixin):
    def __init__(self, name, motor_type, stealth_mode=False):
        super(Plane, self).__init__(name=name, kind_of=self.__class__.__name__, environment='air')
        self.motor_type = motor_type
        self.stealth_mode = stealth_mode
        self.autopilot_mode = False

    def autopilot_on_off(self):
        if self.autopilot_mode:
            print(f'{self.kind_of} "{self.name}" turned off autopilot.')
            self.autopilot_mode = False
        else:
            print(f'{self.kind_of} "{self.name}" is in autopilot mode.')
            self.autopilot_mode = True


class Ship(Transport, MovesMixin):
    def __init__(self, name, displacement=0, armament=False):
        super(Ship, self).__init__(name=name, kind_of=self.__class__.__name__, environment='water')
        self.displacement = displacement
        self.armament = armament

    def fire(self):
        if self.armament:
            print('Boom ... booom ...booooom')


class CarShip(Car, Ship):
    def __init__(self, name, color='White', displacement=0, armament=False):
        self.kind_of = self.__class__.__name__
        self.name = str(name)
        self.environment = 'road and water'
        self.stopping_places = list()
        self.color = color
        self.body_type = 'CarShip'
        self.displacement = displacement
        self.armament = armament


amphibian = CarShip(name='2122', armament=True)
amphibian.beep(3)
amphibian.blink('left')
amphibian.blink('right')
amphibian.goes_to('Amsterdam')
amphibian.goes_to('Rotterdam')
amphibian.get_all_stopping_places()
amphibian.fire()
#
# fiat = Car('Fiat 500', 'Red', 'Hatchback')
# fiat.goes_to('London')
# fiat.goes_to('Paris')
# fiat.get_all_stopping_places()
#
# Transport.get_all_info(fiat)
# abarth = Car('Biposto 695', 'Black', 'Hatchback')
# abarth.goes_to('Milan')
# abarth.goes_to('Rome')
#
# boeing = Plane('Boeing 747', 'Turbojet')
# boeing.goes_to('Moscow')
# boeing.goes_to('Kiev')
# boeing.get_all_stopping_places()
# boeing.get_all_info()
# boeing.autopilot_on_off()
# boeing.autopilot_on_off()
