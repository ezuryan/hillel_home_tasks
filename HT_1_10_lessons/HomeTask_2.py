# ------------------------------------------------
# 1
# user_list = [1, 2, 3, 4, 5, 6, 7]
# user_list.reverse()
# print(user_list[0])
#
# ------------------------------------------------
# 2
# user_list = [1, 2, 3, 4, 5, 6, 7]
# user_number1 = int(input("Enter your number 1: "))
# user_number2 = int(input("Enter your number 2: "))
# print(user_list[user_number1:user_number2])
#
# ------------------------------------------------
# 3
# user_str = input("Enter your string : ")
# temp_set = set(user_str)
# temp_list = list(temp_set)
# temp_list.sort()
# print(temp_list)
#
# ------------------------------------------------
# 4
# tmp_set1 = {1, 2, 3, 4, 5}
# tmp_set2 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
# tmp_list = list(tmp_set1.intersection(tmp_set2))
# print(tmp_list[0]-tmp_list[len(tmp_list)-1])
#
# ------------------------------------------------
# 5
# user_list1 = [1, 2, 3, 4, 5, 6, 7]
# user_list2 = [7, 8, 9, 10, 11, 12, 13]
# tmp_set1 = set(user_list1 + user_list2)
# user_list1 = user_list2 = list(tmp_set1)
# print(user_list1,user_list2,sep='\n')
#
# ------------------------------------------------
# 6
# user_list = [1, 2, 3, 4, 5, 6, 7, 'H']
# user_tuple = (7, 8, 9, 10, 11, 12, 13, 'H')
# tmp_set1 = set(user_list)
# tmp_set2 = set(user_tuple)
# tmp_set1.intersection_update(tmp_set2)
# print(tmp_set1)
#
# ------------------------------------------------
# 7
# user_text = 'Дан текст, найти количество предложений в тексте. Текст простой, без сокращений и прямой речи.   '
# user_text = user_text.rstrip()
# print(user_text.count('. ')+1)
#
# ------------------------------------------------
# 8
# tmp_seq = [3, 5, 7, 11]
# shag1 = tmp_seq[1] - tmp_seq[0]
# shag2 = tmp_seq[-1] - tmp_seq[-2]
# tmp_list = [shag1, shag2]
# tmp_list.sort()
# tmp_set1 = set(tmp_seq)
# tmp_set2 = set(range(tmp_seq[0], tmp_seq[-1]+1, tmp_list[0]))
# tmp_set2 = tmp_set2.difference(tmp_set1)
# print(tmp_set2)
