# 1 ------------------------------------------------------------------------------------
# user_value = input('Enter your digit value: ')
# if len(user_value) != 0:
#     i = user_value.find(',')
#     if i != -1:
#         tmp_str = user_value[:i] + '.' + user_value[i + 1:]
#         user_value = float(tmp_str)
#
#     user_value = float(user_value)
#
#     if user_value % 2 == 0:
#         print(user_value, ' - Even')
#     else:
#         print(user_value, ' - Odd')
# else:
#     print('No value')
#
# 2 ------------------------------------------------------------------------------------
# user_str = (input('Enter values separated with comma: '))
# tmp_lst = list()
# tmp_str = str()
# for i in user_str:
#     if i != ',':
#         tmp_str = tmp_str + i
#     else:
#         tmp_lst.append(tmp_str)
#         tmp_str = ''
#
# if len(tmp_str) != 0:
#     tmp_lst.append(tmp_str)
#
# if len(tmp_lst) < 10:
#     tmp_lst.sort(reverse=True)
# elif len(tmp_lst) > 10:
#     tmp_lst.sort()
# else:  # if = 10
#     print('No sorting conditions!')
#
# if len(tmp_lst) > 0:
#     print(tmp_lst)
# else:
#     print('No value')
#
# 3 ------------------------------------------------------------------------------------
# user_value1 = input('Enter your digit value 1: ')
# user_value2 = input('Enter your digit value 2: ')
#
# if len(user_value1) != 0:
#     i = user_value1.find(',')
#     if i != -1:
#         tmp_str = user_value1[:i] + '.' + user_value1[i + 1:]
#         user_value1 = float(tmp_str)
#
#     user_value1 = float(user_value1)
# else:
#     print('No value 1')
#
# tmp_str = ''
# if len(user_value2) != 0:
#     i = user_value2.find(',')
#     if i != -1:
#         tmp_str = user_value2[:i] + '.' + user_value2[i + 1:]
#         user_value2 = float(tmp_str)
#
#     user_value2 = float(user_value2)
# else:
#     print('No value 2')
#
# if user_value1 > user_value2:
#     print(user_value1 - user_value2)
# elif user_value2 > user_value1:
#     print(user_value1 + user_value2)
# elif user_value1 == user_value2:
#     print(user_value1 ** user_value2)
#
# 4 ------------------------------------------------------------------------------------
# user_number1 = int(input("Enter your number 1: "))
# user_number2 = int(input("Enter your number 2: "))
# user_number3 = int(input("Enter your number 3: "))
#
# tmp_value = (user_number1 - user_number2 + user_number3)
# if tmp_value != 0:
#     print((2 * user_number1 - 8 * user_number2) / tmp_value)
# elif tmp_value == 0:
#     print('Zero division')
#
# 5 ------------------------------------------------------------------------------------
# value_list = [-3, -2, -1, 0, 1, 2, 3, 4]
# x = 0
# for item in value_list:
#     if item % 2 == 0:
#         x += 1
# print(x, 'total - 1 variant')
#
# tmp_list = [value for value in value_list if value % 2 == 0]
# print(len(tmp_list), 'total - 2 variant')
#
# 6 ------------------------------------------------------------------------------------
# value_list = [4, -3, 2, -1, 0, 1, -2, 3, -4, 7, 18, -20, -9, -1.5]
# max_value = max(value_list)
# sort_type = 1  # 0 - от меньшего к большему, 1 - от большего к меньшему
#
# tmp_list = list()
# while len(value_list) != 0:
#     max_value = max(value_list)
#     current_index = value_list.index(max_value)
#     current_value = value_list.pop(current_index)
#     tmp_list.append(current_value)
#
# if sort_type == 0:
#     tmp_list.reverse()
#
# print(tmp_list)
# 7 ------------------------------------------------------------------------------------
# user_number = input("Enter len Fibonachi: ")
# if user_number.isdigit():
#     user_number = int(user_number)
#
#     tmp_list = list()
#     for i in range(0, user_number):
#         ii = i
#         if i > 1:
#             ii = tmp_list[i - 1] + tmp_list[i - 2]
#
#         tmp_list.append(ii)
#
#     print(tmp_list)
# else:
#     print('Error entered value')
#
# 8 ------------------------------------------------------------------------------------
# user_number = input("Enter value: ")  # формула не самая обычная нашел доказательство в матфорумах
# if user_number.isdigit():
#     user_number = int(user_number)
#     if user_number == 1:
#         print(user_number, ' - not a prime number')
#     elif user_number == 2 or user_number == 3:
#         print(user_number, ' - prime number')
#     elif user_number > 3:
#         x = user_number ** 2
#         y = x - 1
#         if y % 24 == 0:
#             print(user_number, ' - prime number')
#         else:
#             print(user_number, ' - not a prime number')
# else:
#     print('Value is not digit')
#
# extra tests
# 1 ------------------------------------------------------------------------------------
# matrix = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
# for y in range(0, len(matrix)):
#     print(matrix[y][0], ' - ', matrix[y][len(matrix[0]) - 1])
#
# 2 ------------------------------------------------------------------------------------
# user_string = input('Enter your string: ')
# tmp_string = ''
# for letter in user_string:
#     if letter.isalpha():
#         tmp_string = tmp_string + letter
# print(tmp_string)
#
# 3 ------------------------------------------------------------------------------------
# user_string = input('Enter your string: ')
# search_symbol = user_string[-1]
#
# # var 1
# counter = 0
# for letter in user_string:
#     if letter == search_symbol:
#         counter += 1
# print('Total count of {0} = {1}'.format(search_symbol, counter))
# print('Count of same as last {0} = {1}'.format(search_symbol, counter - 1))
#
# # var 2
# counter = 0
# flag = 0
# while flag != len(user_string) - 1:
#     flag = user_string.find(user_string[-1], flag + 1)
#     counter += 1
# print('Total count of {0} = {1}'.format(search_symbol, counter))
# print('Count of same as last {0} = {1}'.format(search_symbol, counter - 1))
#

