import os
import pickle


class Books:
    def __init__(self, name, author, year, genre, text_of_the_book):
        self.name = str(name.upper())
        self.author = str(author.title())
        self.year = str(year)
        self.genre = str(genre.upper())
        self.text_of_the_book = str(text_of_the_book)

    def book_detail(self):
        print(f'\nName: {self.name}\nAuthor: {self.author}\nGenre: {self.genre}\nYear: {self.year}')

    def read_book(self):
        self.book_detail()
        print(f'Text of the book:\n{self.text_of_the_book}')

    def print_book(self):
        pass

    def recommend_book(self):
        pass


class HomeLibraries:
    def __init__(self):
        self.name = self.__class__.__name__
        self.books = list()

    def introduction(self, return_books=False):
        print(f'\nThis is a library of "{self.name}"\n'
              f'It has {len(self.books)} books')
        if return_books:
            return self.books

    def add_book(self, book_link):
        if book_link:
            _books = self.get_books(name=book_link.name, author=book_link.author, genre=book_link.genre,
                                    year=book_link.year)
            if len(_books):
                print(f'Already have this book "{book_link.name}"')
            else:
                self.books.append(book_link)
        else:
            print(f'No book to add')

    def delete_book(self, book_link):
        if book_link:
            self.books.remove(book_link)
            print(f'\nThe book "{book_link.name}" - deleted')

    def get_books(self, **kwargs):
        res_list = list()
        if len(self.books) > 0:
            if kwargs:
                _dkey = str()
                try:
                    n = str(kwargs['name'].upper())
                    _dkey += '1'
                except:
                    n = ''
                    _dkey += '0'
                try:
                    a = str(kwargs['author'].title())
                    _dkey += '1'
                except:
                    a = ''
                    _dkey += '0'
                try:
                    y = str(kwargs['year'])
                    _dkey += '1'
                except:
                    y = ''
                    _dkey += '0'
                try:
                    g = str(kwargs['genre'].upper())
                    _dkey += '1'
                except:
                    g = ''
                    _dkey += '0'

                _lkey = n + a + y + g

                res_list = list(filter(
                    lambda x: str(str(x.name) * int(_dkey[0]) + \
                                  str(x.author) * int(_dkey[1]) + \
                                  str(x.year) * int(_dkey[2]) + \
                                  str(x.genre) * int(_dkey[3])) == _lkey, self.books))
            else:
                res_list.append(self.books[-1])

        return res_list

    def save_dump(self):
        # dir = os.path.abspath(os.curdir)
        file_name = f'{self.__class__.__name__}.dump'
        with open(f'{file_name}', 'wb') as dump_file:
            pickle.dump(self, dump_file)
            print(f'File "{file_name}" saved')

    def load_dump(self):
        # dir = os.path.abspath(os.curdir)
        file_name = f'{self.__class__.__name__}.dump'
        if os.path.exists(f'{file_name}'):
            with open(f'{file_name}', 'rb') as dump_file:
                self = pickle.load(dump_file)
        else:
            print('No dump file')
        return self


# первый кейс создание
my_personal_library = HomeLibraries()
my_personal_library.load_dump()

# # второй кейс создание
# my_personal_library = None
# my_personal_library = HomeLibraries().load_dump()

# наполнение
book1 = Books('book1', 'author 1', 2006, 'genre1', 'text....')
book2 = Books('book2', 'author 2', 2005, 'genre2', 'text....')
book3 = Books('book3', 'author 3', 2004, 'genre3', 'text....')
book4 = Books('book4', 'author 1', 2003, 'genre3', 'text....')
book5 = Books('book5', 'author 2', 2002, 'genre2', 'text....')
book6 = Books('book6', 'author 2', 2001, 'genre4', 'text....')

# функции
my_personal_library.add_book(book1)
my_personal_library.add_book(book2)
my_personal_library.add_book(book3)
my_personal_library.add_book(book4)
my_personal_library.add_book(book5)
my_personal_library.add_book(book6)

my_personal_library.save_dump()

# my_personal_library.introduction()


# books = my_personal_library.introduction(True)
# for book in books:
#     Books.book_detail(book)

"""
get_books() фильтрует по любой комбинации, в любом порядке
"""
# # 1 вариант
# books = my_personal_library.get_books()
# for book in books:
#     Books.book_detail(book)
#
# # 2 вариант
# books = my_personal_library.get_books(genre='genre2')
# for book in books:
#     Books.book_detail(book)
#
# # 3 вариант
# books = my_personal_library.get_books(author='author 2', genre='genre2',  year=2005)
# for book in books:
#     Books.book_detail(book)
#
#
# book = my_personal_library.get_books(name='book1')
# if len(book):
#     HomeLibraries.delete_book(my_personal_library, book[0])
