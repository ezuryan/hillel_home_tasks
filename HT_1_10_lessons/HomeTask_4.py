# # шифр Цезаря
# import string
#
#
# def make_cipher(bias=0):
#     _alfabet = string.ascii_uppercase
#     res_dict = dict()
#     if bias < len(_alfabet):
#         _alfabet_cipher = _alfabet[bias:] + _alfabet[:bias]
#         i = 0
#         while i <= len(_alfabet) - 1:
#             res_dict.update({_alfabet[i]: _alfabet_cipher[i]})
#             res_dict.update({_alfabet[i].lower(): _alfabet_cipher[i].lower()})
#             i += 1
#         return res_dict
#     else:
#         return None
#
#
# def cipher_text(origanal_text, cipher):
#     cipher_text = str()
#     for letter in origanal_text:
#         if letter.isalpha():
#             next_letter = cipher.get(letter)
#         else:
#             next_letter = letter
#         cipher_text = cipher_text + next_letter
#     return cipher_text
#
#
# user_bias = input('Enter bias value: ')
# if user_bias.isdigit():
#     user_bias = int(user_bias)
#     res_cipher_dict = make_cipher(user_bias)
#
#     if res_cipher_dict is not None:
#         with open('Files_for_tests\Caesar.txt', 'r') as file_before:
#             text_before = file_before.read()
#             text_after = cipher_text(text_before, res_cipher_dict)
#
#         with open('Files_for_tests\Caesar_cipher.txt', 'w') as file_after:
#             file_after.write(text_after)
#
#         print('Success')
#     else:
#         print('Not cipher, incorrect bias value')
# else:
#     print('bias value error')
