import sqlite3
from flask import Flask, render_template, request, redirect

app = Flask(__name__, template_folder='templates')


@app.route('/')
def index():
    with sqlite3.connect('database.db') as connection:
        query = f'SELECT * FROM music_lib'

        cursor = connection.cursor()
        cursor.execute(query)
        lib = cursor.fetchall()
    return render_template('index.html', lib=lib)


@app.route('/add_song/', methods=['GET', 'POST'])
def add_song():
    if request.method == 'POST':
        with sqlite3.connect('database.db') as connection:
            query = f'INSERT INTO music_lib (Track_name, Artist, Duration)' \
                    f'VALUES (' \
                    f'"{request.form["track_name"]}",' \
                    f'"{request.form["artist"]}",' \
                    f'"{request.form["duration"]}")'

            cursor = connection.cursor()
            cursor.execute(query)
            connection.commit()
            return redirect('/')
    return render_template('add_song.html')


@app.route('/delete_song/', methods=['GET', 'POST'])
def delete_song():
    if request.method == 'POST':
        with sqlite3.connect('database.db') as connection:
            query = f"DELETE FROM music_lib WHERE id={request.form['song_id']}"

            cursor = connection.cursor()
            cursor.execute(query)
            connection.commit()
            return redirect('/')


@app.route('/edit_song/<int:song_id>', methods=['GET', 'POST'])
def edit_song(song_id):
    with sqlite3.connect('database.db') as connection:
        query = f'SELECT * FROM music_lib WHERE id={song_id}'

        cursor = connection.cursor()
        cursor.execute(query)
        song = cursor.fetchall()[0]

    if request.method == 'POST':
        with sqlite3.connect('database.db') as connection:
            query = f'UPDATE music_lib SET ' \
                    f'Track_name = "{request.form["track_name"]}", ' \
                    f'Artist     = "{str(request.form["artist"]).upper()}", ' \
                    f'Duration   = "{request.form["duration"]}" ' \
                    f'WHERE id   = {song_id}'

            cursor = connection.cursor()
            cursor.execute(query)
            connection.commit()
            return redirect('/')

    return render_template('edit_song.html', song=song)


if __name__ == '__main__':
    app.run(debug=True)
