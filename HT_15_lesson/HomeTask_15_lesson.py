import asyncio
from functools import reduce

import aiofiles
import time
import os


async def operation(lines):
    _str = lines[1].replace(',', '.')

    try:
        _op = int(lines[0])
        _lst = [float(x) for x in _str.split(' ')]
    except:
        _lst = []

    if _lst:
        if _op == 1:  # сложение
            return reduce(lambda x, y: x + y, _lst)
        elif _op == 2:  # умножение
            return reduce(lambda x, y: x * y, _lst)
        elif _op == 3:  # сумма квадратов
            return reduce(lambda x, y: x + y ** 2, _lst) - _lst[0] + (_lst[0]) ** 2
        else:
            assert False, f'Can not be operation number: {_op}'


async def read_file(file_name, main_res_list):
    async with aiofiles.open(file_name) as file:
        lines = await file.readlines()
        res = await operation(lines)

        if res:
            main_res_list.append((file_name, res))
        else:
            print(f'File: {file_name} will not be used, because of template error')


async def main():
    tasks = []
    main_res_list = []
    _sum = 0

    try:
        file_list = os.listdir('Data')
    except:
        print(f'No folder "Data"')
        file_list = []

    if file_list:
        file_list = [os.path.join('Data', x) for x in os.listdir('Data') if x.rfind('.dat') > 0]
        for file_name in file_list:
            _task = asyncio.ensure_future(read_file(file_name, main_res_list))
            tasks.append(_task)

        await asyncio.gather(*tasks)
    else:
        print(f'No .dat files in folder "Data"')

    if main_res_list:
        for i in main_res_list:
            _sum += i[1]

    file_name = os.path.join('Data', 'out.dat')
    try:
        with open(f'{file_name}', 'w') as dump_file:
            dump_file.write(f'Used data: {main_res_list}\n')
            dump_file.write(f'Sum: {_sum}\n')
    except:
        pass


if __name__ == '__main__':
    t1 = time.time()
    event_loop = asyncio.get_event_loop()
    event_loop.run_until_complete(main())

    print(f'Done in {time.time() - t1} sec.')
