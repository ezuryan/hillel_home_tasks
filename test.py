# import asyncio
# import time
#
# import asyncio
#
# async def compute(x, y):
#     print("Compute %s + %s ..." % (x, y))
#     await asyncio.sleep(1.0)
#     return x + y
#
# async def print_sum(x, y):
#     result = await compute(x, y)
#     print("%s + %s = %s" % (x, y, result))
#
# loop = asyncio.get_event_loop()
# loop.run_until_complete(print_sum(1, 2))
# loop.close()

# import asyncio
# import aiohttp
#
# urls = ['http://www.google.com', 'http://www.yandex.ru', 'http://www.python.org']
#
# async def call_url(url):
#     print('Starting {}'.format(url))
#     response = await aiohttp.ClientSession().get(url)
#     data = await response.text()
#     print('{}: {} bytes: {}'.format(url, len(data), data))
#     return data
#
# futures = [call_url(url) for url in urls]
#
# loop = asyncio.get_event_loop()
# loop.run_until_complete(asyncio.wait(futures))
#
#
#
#
#
# # # # from functools import wraps
# # # #
# # # #
# # # # def skip(value=None):
# # # #     def dec_outer(func):
# # # #         # @wraps(func)
# # # #         def wrapper(*args, **kwargs):
# # # #             if value:
# # # #                 kwargs['param'] = value
# # # #                 return func(*args, **kwargs)
# # # #             else:
# # # #                 print(f'Function skipped')
# # # #
# # # #         return wrapper
# # # #
# # # #     return dec_outer
# # # #
# # # #
# # # # # без декоратора
# # # # def my_function(param=None):
# # # #     print(f'Param from {param}')
# # # #
# # # #
# # # # my_function(param='func')
# # # #
# # # #
# # # # # пропускает выполненние
# # # # @skip()
# # # # def my_function(param=None):
# # # #     print(f'Param from {param}')
# # # #
# # # #
# # # # my_function(param='func')
# # # #
# # # #
# # # # # передает параметр в функцию
# # # # @skip('decor')
# # # # def my_function(param=None):
# # # #     print(f'Param from {param}')
# # # #
# # # #
# # # # my_function(param='func')
# # #
# # # # take second element for sort
# # # def takeSecond(elem):
# # #     return elem[1]
# # #
# # #
# # # # random list
# # # random = [(2, 2), (3, 4), (4, 1), (1, 3)]
# # #
# # # # sort list with key
# # # random.sort(key=takeSecond)
# # #
# # # # print list
# # # print('Sorted list:', random)
# #
# # mydict = {
# #     'name': '1',
# #     'author': 2,
# #     'genre': 3
# # }
# #
# # print(mydict['name'])
#
#
#
#
# # Example Python program that constructs a date object
# # from a string containing a date in ISO format. i.e., YYYY-DD-MM
#
# # import datetime
# #
# # # Birth day of Picaso
# # birthday_Picaso = "1881-10-25";
# #
# # # Construct a datetime.date object
# # picasoBirthDate = datetime.date.fromisoformat(birthday_Picaso);
# # print("Birth day of Picaso: %s"%picasoBirthDate);
#
