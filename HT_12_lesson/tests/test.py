import unittest
from unittest import TestCase
from unittest.mock import patch

from HT_12_lesson.HomeTask_11_lesson_copy import AccountingTable, datetime


class AccountingTableTestCase(TestCase):
    def setUp(self):
        prclst = {
            'product_name': 'Prod1',
            'start_date': datetime.now(),
            'price_cost': 10,
            'price_sell': 20}
        trnsctn = {
            'date': datetime.now(),
            'product_name': 'Prod1',
            'before': 0,
            'income': 100,
            'outgo': 70,
            'after': 30,
            'price_cost': 10,
            'price_sell': 20}

        self.table = AccountingTable()
        self.table.product_names_set = {'Prod1', 'Prod2'}
        self.table.price_list.append(prclst)
        self.table.transactions.append(trnsctn)

    def test_product_exists(self):
        res = self.table.product_exists(product_name='Prod1')
        self.assertTrue(res, msg='The function "product_exists" - not working')

    def test_get_price(self):
        res = self.table.get_price(product_name='Prod1')
        self.assertIsInstance(res, tuple)
        self.assertTrue(len(res) == 2, msg='The function "get_price" have to return 2 values in tuple')
        self.assertEqual(sum(res), 30, msg='The function "get_price" - not working')

    def test_get_balance(self):
        res1 = self.table.get_balance(product_name='Prod1')
        self.assertEqual(res1, 30, msg='The function "get_balance" - not working')

        res2 = self.table.get_balance(product_name='Prod2')
        self.assertEqual(res2, 0, msg='The function "get_balance" - not working')

    def test_add_transaction(self):

        pass


if __name__ == '__main__':
    unittest.main()
