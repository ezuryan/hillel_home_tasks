import os
import pickle
from datetime import datetime
from itertools import groupby
from time import sleep


class AccountingTable:
    def __init__(self):
        self.transactions = list()
        self.product_names_set = set()
        self.price_list = list()

    def product_exists(self, product_name: str):
        product_name = str(product_name).capitalize()
        if product_name in self.product_names_set:
            return True
        else:
            return False

    def get_price(self, product_name: str, price_date: str = ''):
        product_name = str(product_name).capitalize()
        if price_date:
            actual_date = datetime.strptime(price_date, "%Y-%m-%d")
        else:
            actual_date = datetime.now()

        price_cost = 0
        price_sell = 0

        res = list(
            filter(lambda x: (x['start_date'] <= actual_date and x['product_name'] == product_name), self.price_list))
        if res:
            res = sorted(res, key=lambda x: x['start_date'])
            price_cost = res[-1].get('price_cost')
            price_sell = res[-1].get('price_sell')
        # else:
        # print(f'\nNo price for this date: {actual_date}')

        return price_cost, price_sell


    def get_balance(self, product_name: str):
        product_name = str(product_name).capitalize()

        if self.product_exists(product_name):
            res = list(filter(lambda x: x['product_name'] == product_name, self.transactions))
            if res:
                res = sorted(res, key=lambda x: x['date'])
                res = res[-1].get('after')
            else:
                res = 0
            return res

    def add_transaction(self, product_name, before, income, outgo, after):
        price = self.get_price(product_name=product_name)

        if sum(price) == 0:
            print(f'Can not add transaction without prices. Please set price for {product_name}')
        else:
            sleep(0.5)  # для наглядности
            trnsctn = {
                # 'id': uuid.uuid1(),
                'date': datetime.now(),
                'product_name': product_name,
                'before': int(before),
                'income': int(income),
                'outgo': int(outgo),
                'after': int(after),
                'price_cost': price[0],
                'price_sell': price[1]
            }
            self.transactions.append(trnsctn)

    def product_new(self, product_name: str, price_cost: float = 0, price_sell: float = 0):
        try:
            product_name = str(product_name).capitalize()
            price_cost = round(float(price_cost), 1)
            price_sell = round(float(price_sell), 1)
        except:
            print(f'Value error')
            return None

        if not self.product_exists(product_name):
            self.product_names_set.add(product_name)
            self.set_price(product_name=product_name, price_cost=price_cost, price_sell=price_sell)
        else:
            print(f'\nProduct "{product_name}" already exists')

    def set_price(self, product_name: str, price_cost: float = 0, price_sell: float = 0, start_date: str = ''):
        try:
            product_name = str(product_name).capitalize()
            price_cost = round(float(price_cost), 1)
            price_sell = round(float(price_sell), 1)
            if start_date:
                start_date = datetime.strptime(start_date, "%Y-%m-%d")
            else:
                start_date = datetime.now()
        except:
            print(f'Value error')
            return None

        if price_cost != 0 and price_sell != 0:
            self.price_list.append({
                'product_name': product_name,
                'start_date': start_date,
                'price_cost': price_cost,
                'price_sell': price_sell
            })

    def product_income(self, product_name: str, amount: int):
        try:
            product_name = str(product_name).capitalize()
            amount = int(amount)
        except:
            print(f'Value error')
            return None

        if self.product_exists(product_name):
            _before = self.get_balance(product_name)
            self.add_transaction(product_name=product_name,
                                   before=_before,
                                   income=amount,
                                   outgo=0,
                                   after=_before + int(amount))

    def product_outgo(self, product_name: str, amount: int):
        try:
            product_name = str(product_name).capitalize()
            amount = int(amount)
        except:
            print(f'Value error')
            return None

        if self.product_exists(product_name):
            _before = self.get_balance(product_name)
            _after = _before - int(amount)
            if _before > 0 and _after >= 0:
                self.add_transaction(product_name=product_name,
                                       before=_before,
                                       income=0,
                                       outgo=amount,
                                       after=_before - int(amount))
            else:
                print(f'\nNot enough {product_name}: {_before} - left, {amount} - necessary')

    def load_dump(self):
        file_name = f'{self.__class__.__name__}.dump'
        if os.path.exists(f'{file_name}'):
            with open(f'{file_name}', 'rb') as dump_file:
                res = pickle.load(dump_file)
                return res
        else:
            print(f'\nNo dump file. DB is empty')
            return self



    def save_dump(self):
        file_name = f'{self.__class__.__name__}.dump'
        with open(f'{file_name}', 'wb') as dump_file:
            pickle.dump(self, dump_file)
            # print(f'\nFile "{file_name}" saved')

    def show_report(self, period):
        res = input(f'To show full report enter "y" (or leave blank for brief): ')
        if res == 'y' or res == 'Y':
            full = True
        else:
            full = False

        flg = ''
        if period == 'daily':
            flg = '%d-%m-%Y'
        elif period == 'weekly':
            flg = '%W-%Y'
        elif period == 'monthly':
            flg = '%m-%Y'
        elif period == 'annually':
            flg = '%Y'
        print(f'\n{period.upper()} REPORT start --------------------------')
        # tmp_list = self.transactions.copy()
        tmp_list = sorted(self.transactions, key=lambda x: (x['date'].strftime(flg), x['product_name'], x['date']))
        for key, group_items in groupby(tmp_list, key=lambda x: x['date'].strftime(flg)):
            print(f'Period: {key}')
            for _key, _group_items in groupby(group_items, key=lambda x: x['product_name']):
                print(f'  Product: {_key}')

                if not full:
                    _group_items = list(_group_items)
                    income = outgo = _income = _outgo = 0
                    for item in _group_items:
                        income += item['income']
                        _income += item['income'] * item['price_cost']
                        outgo += item['outgo']
                        _outgo += item['outgo'] * item['price_sell']

                    print(
                        f'    before: [{_group_items[0]["before"]}][{_group_items[0]["before"] * _group_items[0]["price_cost"]} $]'
                        f'\n    income: [{income}][{_income} $]'
                        f'\n    outgo:  [{outgo}][{_outgo} $]'
                        f'\n    after:  [{_group_items[-1]["after"]}][{_group_items[-1]["after"] * _group_items[-1]["price_cost"]} $]'
                    )

                else:
                    for item in _group_items:
                        print(f'    '
                              f'before: [{item["before"]}][{item["before"] * item["price_cost"]} $] - '
                              f'income: [{item["income"]}][{item["income"] * item["price_cost"]} $] - '
                              f'outgo: [{item["outgo"]}][{item["outgo"] * item["price_sell"]} $] - '
                              f'after: [{item["after"]}][{item["after"] * item["price_cost"]} $]'
                              )
        print(f'{period.upper()} REPORT end --------------------------')

    def products_prices(self):
        print(self.product_names_set)
        tmp_list = sorted(self.price_list, key=lambda x: (x['product_name'], x['start_date']))
        for i in tmp_list:
            print(i)


if __name__ == '__main__':
    pass
