from HT_11_lesson.HomeTask_11_lesson import AccountingTable

"""Разработка приложения для предметной области «Учёт товаров в магазине» Разработать приложение, позволяющее 
собирать и накапливать сведения о поступлении и реализации товаров некоторого магазина. Предусмотреть методы для 
вывода на экран отчетов, за день, за неделю, за месяц и за год. Отчеты короткие - приход, расход, прибыль. Учтите что 
программа рассчитана на долгое использование, то есть, после перезапуска программы данные не должны быть утеряны. """

params = {
    1: 'New product',
    2: 'Income product',
    3: 'Change product prices',
    4: 'Sell product',
    5: 'Show daily report',
    6: 'Show weekly report',
    7: 'Show monthly report',
    8: 'Show annually report',
    9: 'Show all products and prices',
    10: 'Exit'
}


def make_choice(_cycle):
    print('')
    if _cycle == 1:
        for k, v in params.items():
            print(f'{k}: {v}')

    res = input(f'Make your choice: ')

    try:
        res = int(res)
    except:
        print('Choice error')
        return make_choice(1)

    if params.get(res):
        print(params.get(res))
        return res
    else:
        print('Choice error')
        return make_choice(1)


if __name__ == '__main__':
    cycle = 0
    while True:
        cycle += 1
        if cycle == 1:
            _table = AccountingTable().load_dump()

        choice = make_choice(cycle)
        if choice == 10:
            _table.save_dump()
            break
        if choice == 9:
            _table.products_prices()
        elif choice == 8:
            _table.show_report(period='annually')
        elif choice == 7:
            _table.show_report(period='monthly')
        elif choice == 6:
            _table.show_report(period='weekly')
        elif choice == 5:
            _table.show_report(period='daily')
        elif choice == 4:
            product_name = input(f'Input product name: ')
            amount = input(f'Input amount to sell: ')
            _table.product_outgo(product_name=product_name, amount=amount)
        elif choice == 3:
            product_name = input(f'Input product name: ')
            price_cost = input(f'Input product price_cost: ')
            price_sell = input(f'Input product price_sell: ')
            start_date = input(f'Input price start date (or leave blank) (format = %Y-%m-%d): ')
            _table.set_price(product_name=product_name, price_cost=price_cost, price_sell=price_sell, start_date=start_date)
        elif choice == 2:
            product_name = input(f'Input product name: ')
            amount = input(f'Input amount to income: ')
            _table.product_income(product_name=product_name, amount=amount)
        elif choice == 1:
            product_name = input(f'Input new product name: ')
            price_cost = input(f'Input product price_cost: ')
            price_sell = input(f'Input product price_sell: ')
            _table.product_new(product_name=product_name, price_cost=price_cost, price_sell=price_sell)

    _table.save_dump()  # для тесторования
